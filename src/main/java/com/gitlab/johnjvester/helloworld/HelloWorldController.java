package com.gitlab.johnjvester.helloworld;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class HelloWorldController {
    @GetMapping(value = "/hello")
    public String getMessage() {
        return "Hello World! The current date/time is " + LocalDateTime.now() + ".";
    }
}
