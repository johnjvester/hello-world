FROM openjdk:8-jdk-alpine
RUN apk update
RUN apk add --no-cache bash curl jq

WORKDIR /app

COPY docker/scripts/run.sh /app/run.sh

RUN adduser -D appuser
RUN chown -R appuser:appuser /app

USER appuser

RUN sed -i 's/\r$//g' run.sh

COPY target/helloWorld.jar helloWorld.jar

CMD ./run.sh
