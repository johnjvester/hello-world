# `hello-world`

[![pipeline status](https://gitlab.com/johnjvester/hello-world/badges/master/pipeline.svg)](https://gitlab.com/johnjvester/hello-world/commits/master)

> The `hello-world` repository contains a [Java](<https://en.wikipedia.org/wiki/Java_(programming_language)>) based
> application, utilizing the [Spring Boot](<https://spring.io/projects/spring-boot>) framework and is intended to
> provide a working example a simple [RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) service 
> that can be deployed via [Docker](https://en.wikipedia.org/wiki/Docker_(software)).

## Using `hello-world`

A single URI exists, `/hello` which returns the following data as a `String` object:

```text
Hello World! The current date/time is 2021-03-18T10:17:43.403.
```

The Spring Book Actuator (`/actuator`), which is also available.

The default port for this `hello-world` service is `8888`.

## Building `hello-world`

Simply use the following command to build the `hello-world` service:

```commandline
./mvnw -U clean package
```

This will result in the following `.jar` file being created:

```commandline
/target/helloWorld.jar
```

Java can then be utilized to run the `hello-world` Spring Boot service:

```commandline
java -jar /target/helloWorld.jar
```

## Using Docker

There is a `Dockerfile` which works with the `docker/scripts/run.sh` shell script to build the `hello-world` service 
so that the service can be utilized within Docker, including `docker-compose`. 

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.