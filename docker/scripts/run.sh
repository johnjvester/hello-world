#!/usr/bin/env bash
set -e

EXTRA_JVM_PARAMS=${EXTRA_JVM_PARAMS:-""}

echo Starting hello-world Service
echo
echo "Using Extra JVM Params:"
echo "----------------------"
echo "${EXTRA_JVM_PARAMS// /$'\n'}"
echo

set -x

exec java -noverify -server ${EXTRA_JVM_PARAMS} -jar /app/helloWorld.jar
